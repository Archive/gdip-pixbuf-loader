#! /bin/sh

# back in the stupidity of autoreconf
if [ ! -z "$ACLOCAL_FLAGS" ] ; then
    [ -z "$ACLOCAL" ] && ACLOCAL=aclocal
    ACLOCAL="$ACLOCAL $ACLOCAL_FLAGS"
    export ACLOCAL
fi
autoreconf -v --install || exit 1

./configure "$@"
