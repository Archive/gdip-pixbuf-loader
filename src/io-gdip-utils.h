/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */
/* GdkPixbuf library - Win32 GDI+ Pixbuf Loader
 *
 * Copyright (C) 2008 Dominic Lachowicz
 * Copyright (C) 2008 Alberto Ruiz
 *
 * Authors: Dominic Lachowicz <domlachowicz@gmail.com>
 *          Alberto Ruiz <aruiz@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more  * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef _HAVE_IO_GDIP_UTILS_H
#define _HAVE_IO_GDIP_UTILS_H

#define GDK_PIXBUF_ENABLE_BACKEND
#include <gdk-pixbuf/gdk-pixbuf-io.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include "gdk-pixbuf-private.h"

#include "io-gdip-native.h"

#ifndef INCLUDE_gdiplus
#define MODULE_ENTRY(type,function) G_MODULE_EXPORT function
#else
#define MODULE_ENTRY(type,function) _gdk_pixbuf__ ## type ## _ ## function
#endif

gboolean
gdip_save_to_file_callback (const gchar *buf,
                            gsize        count,
                            GError     **error,
                            gpointer     data);

void
gdip_fill_vtable (GdkPixbufModule *module);

void
gdip_fill_vector_vtable (GdkPixbufModule *module);

gboolean
gdip_save_pixbuf (GdkPixbuf *pixbuf,
                  const WCHAR *format,
                  const EncoderParameters *encoder_params,
                  GdkPixbufSaveFunc save_func,
                  gpointer user_data,
                  GError **error);

#endif
