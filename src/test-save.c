#include <stdio.h>
#include <stdlib.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

int main(int argc, char **argv)
{
  GdkPixbuf *pixbuf;
  GError *error = NULL;

  if (argc != 3)
    {
      g_print("Usage: test-save.exe in.png out.png\n");
      exit(1);
    }
  
  g_type_init();

  pixbuf = gdk_pixbuf_new_from_file (argv[1], &error);
  if (!pixbuf)
    {
      g_print("Error loading: %s\n", error->message);
      g_error_free(error);
      exit(1);
    }

  if (!gdk_pixbuf_save(pixbuf, argv[2], "png", &error, NULL))
    {
      g_print("Error saving: %s\n", error->message);
      g_error_free(error);
      exit(1);
    }

  return 0;
}
